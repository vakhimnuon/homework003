package controllers

import (
	"context"
	"homework/initializers"
	"homework/models"
	//  "homework/response"
	"net/http"
		"fmt"
	"github.com/gin-gonic/gin"
	// "github.com/go-kivik/kivik"
)

var body struct {
		ID string `json:"id"`
		Name  string `json:"name"`
		Age int `json:"age"`
	}

type stuResponse struct {
		ID    string `json:"_id"`
		Name  string `json:"name"`
		Age   int    `json:"age"`
		Class string `json:"class"`
	}

func PostStudent(c *gin.Context) {
	// Bind request data to the 'body' struct
	if err := c.Bind(&body); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create a new student record
	id, _, err := initializers.DBconnection.CreateDoc(context.TODO(), body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Return the newly created student ID in the response
	c.JSON(http.StatusCreated, gin.H{"id": id})
}

func FindStudentById(c *gin.Context) {
	id := c.Param("id")

	// Retrieve the student record by ID
	var stu models.Student
	err := initializers.DBconnection.Get(context.TODO(), id).ScanDoc(&stu)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Student not found"})
		return
	}
}


func FindById(id string) models.Student {

	var stu models.Student
	err := initializers.DBconnection.Get(context.TODO(), id).ScanDoc(&stu)
  
	if err != nil {
	  panic(err)
	}
  
	return stu
  }
  func DeleteStudent(c *gin.Context) {

	id := c.Param("id")
	rev := c.Param("rev")
  
	newRev, err := initializers.DBconnection.Delete(context.TODO(), id, rev)
  
	fmt.Print(newRev)
	if err != nil {
	  panic(err)
	}
  
	c.JSON(200, gin.H{
	  "status":  "success",
	  "message": "deleted",
	})
  }