package initializers

import (
	"context"
	_ "github.com/go-kivik/couchdb" // CouchDB driver
	"github.com/go-kivik/couchdb/v3"
	"github.com/go-kivik/kivik"
)
 var DBconnection *kivik.DB
func ConnectToDB() () {
    client, err := kivik.New("couch", "http://admin:123@localhost:5959/")
    if err != nil {
        panic(err) 
    }
    client.Authenticate(context.TODO(),couchdb.BasicAuth("admin", "123"))
	DBconnection = client.DB(context.TODO(),"student")
}