package models
type Student struct {
	ID    string `json:"_id"`
    Rev   string `json:"_rev"`
    Name  string `json:"name"`
    Age   int    `json:"age"`
    Class string `json:"class"`
  }