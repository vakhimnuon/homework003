package main

import (
	// "context"
	"fmt"
	"homework/controllers"
	"homework/initializers"

	// "github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin"
	_ "github.com/go-kivik/couchdb" // CouchDB driver
	// "golang.org/x/net/route"
	// "github.com/go-kivik/kivik"
)

func init() {
	initializers.ConnectToDB()
    fmt.Println("Connected to the database.")
}
func BuildRoute(route *gin.Engine){
	router := route.Group("api/v1/student")

	router.POST("/create", controllers.PostStudent)
	router.GET("/byid/:id", controllers.FindStudentById)
	router.DELETE("delete/:id/:rev",controllers.DeleteStudent)
}
func main() {
	route := gin.Default()
	BuildRoute(route)
	route.Run()
}
