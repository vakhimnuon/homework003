package request

type StuRequest struct {
    Name     string `json:"name" binding:"required"`
    Age      int    `json:"age" binding:"required"`
    Class    string `json:"class" binding:"required"`
}