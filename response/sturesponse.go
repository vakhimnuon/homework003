package response

// StuResponse represents the structure of responses for student records.
type StuResponse struct {
    ID    string `json:"_id"`
    Name  string `json:"name"`
    Age   int    `json:"age"`
    Class string `json:"class"`
    // Add other fields as needed for your student record responses
}